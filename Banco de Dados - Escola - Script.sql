﻿-- Database: escola

-- DROP DATABASE escola;

CREATE DATABASE escola
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'pt_BR.UTF-8'
       LC_CTYPE = 'pt_BR.UTF-8'
       CONNECTION LIMIT = -1;

CREATE TABLE Turma(
	id_turma SERIAL NOT NULL PRIMARY KEY,
	semestre INT NOT NULL,
	codigo_turma CHAR(5) NOT NULL UNIQUE,
	serie VARCHAR(20) NOT NULL 
 );

  CREATE TABLE 	Aluno(
	id_aluno SERIAL PRIMARY KEY NOT NULL,
	cpf CHAR(11) UNIQUE,
	cr DECIMAL,
	nome VARCHAR(50) NOT NULL,
	matricula CHAR(5) NOT NULL UNIQUE,
	fk_id_turma INT REFERENCES Turma(id_turma)
 );

CREATE TABLE Sala (
	id_sala SERIAL PRIMARY KEY NOT NULL,
	andar VARCHAR(30) NOT NULL,
	numero INT NOT NULL,
	complemento VARCHAR(15) NOT NULL,
	capacidade INT NOT NULL
);

CREATE TABLE Professor (
	id_professor SERIAL PRIMARY KEY NOT NULL, 
	matricula INT UNIQUE NOT NULL,
	nome VARCHAR(50) NOT NULL,
	cpf CHAR(11) NOT NULL UNIQUE
);

CREATE TABLE Disciplina (
	id_disciplina SERIAL PRIMARY KEY NOT NULL,
	nome VARCHAR(50) NOT NULL,
	ementa TEXT
);

CREATE TABLE Leciona (
	id_leciona SERIAL PRIMARY KEY NOT NULL,
	fk_id_professor INT REFERENCES Professor (id_professor),
	fk_id_disciplina INT REFERENCES Disciplina (id_disciplina)
);

CREATE TABLE Aula (
	id_aula SERIAL PRIMARY KEY NOT NULL,
	horario TIME NOT NULL,
	fk_id_sala INT REFERENCES Sala (id_sala),
	fk_id_leciona INT REFERENCES Leciona (id_leciona)
);

CREATE TABLE Assiste (
	id_assiste SERIAL PRIMARY KEY NOT NULL,
	fk_id_aula INT REFERENCES Aula (id_aula),
	fk_id_aluno INT REFERENCES Aluno (id_aluno)
);